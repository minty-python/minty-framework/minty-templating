import re

from jinja2 import BaseLoader, Environment, contextfunction, is_undefined
from jsonpath2 import path as jpath

from . import filters


def finalizer(template_value):
    if is_undefined(template_value):
        return ""
    elif isinstance(template_value, list):
        template_value_list = [
            final_value
            for final_value in (finalizer(value) for value in template_value)
            if final_value
        ]

        return ", ".join(template_value_list)

    return template_value


@contextfunction
def _jsonpath_handler(context, selector):
    jp = jpath.Path.parse_str(selector)
    matches = [m.current_value for m in jp.match(context)]

    if len(matches) == 0:
        return ""
    elif len(matches) == 1:
        return matches[0]
    else:
        return matches


class LegacyTemplate:
    _legacy_tag = re.compile(r"\[\[.*?\]\]")

    def __init__(self):
        self.environment = Environment(
            finalize=finalizer,
            loader=BaseLoader(),
            variable_start_string="[[",
            variable_end_string="]]",
        )
        self.environment.globals = {"j": _jsonpath_handler}
        self.environment.filters = filters.legacy_filters

    def render(self, template, context: dict):
        result = template

        # TODO: preprocess: 'itereer:' to array joins

        iterations_left = 10
        while self._legacy_tag.search(result) and iterations_left > 0:
            iterations_left = iterations_left - 1

            template = self.environment.from_string(result)
            result = template.render(context)

        return result

from datetime import date, datetime

from babel.dates import format_date, format_datetime, get_timezone

DEFAULT_DATE_FORMAT = "d MMMM yyyy"
DEFAULT_TZ = "Europe/Amsterdam"


def linebreak_after(template_value):
    return template_value + "\n"


def date_format(template_value, fmt=DEFAULT_DATE_FORMAT):
    date_value = None
    try:
        date_value = date.fromisoformat(template_value)
        return format_date(date_value, format=fmt, locale="nl")
    except ValueError:
        try:
            date_value = datetime.fromisoformat(template_value)

            timezone = get_timezone(DEFAULT_TZ)
            return format_datetime(
                date_value, format=fmt, tzinfo=timezone, locale="nl"
            )
        except ValueError:
            return ""


legacy_filters = {"date": date_format, "break": linebreak_after}

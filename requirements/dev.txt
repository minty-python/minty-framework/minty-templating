## Code style tools
black==v19.3b0
flake8==3.7.9
isort==4.3.10

rope==0.16.0

# Release
bumpversion==0.5.3

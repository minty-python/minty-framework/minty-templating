# from unittest import mock
from minty_templating.template import LegacyTemplate


def test_legacy_template_sunnyday():
    t = LegacyTemplate()
    result = t.render("""[[ a ]] [[ b ]]""", {"a": "b", "b": ["c", "d"]})
    assert result == "b c, d"


def test_legacy_template_sunnyday_nested():
    t = LegacyTemplate()
    result = t.render("""[[ a ]]""", {"a": "[[ b ]]", "b": "c"})
    assert result == "c"


def test_legacy_template_undefined():
    t = LegacyTemplate()
    result = t.render("""[[[ a ]]]""", {"b": "c"})
    assert result == ""


def test_legacy_template_sunnyday_nested_toomuch():
    t = LegacyTemplate()
    result = t.render(
        """[[ a ]]""",
        {
            "a": "[[ b ]]",
            "b": "[[ c ]]",
            "c": "[[ d ]]",
            "d": "[[ e ]]",
            "e": "[[ f ]]",
            "f": "[[ g ]]",
            "g": "[[ h ]]",
            "h": "[[ i ]]",
            "i": "[[ j ]]",
            "j": "[[ k ]]",  # This one won't be processed
        },
    )
    assert result == "[[ k ]]"


def test_legacy_template_filters():
    t = LegacyTemplate()
    result = t.render(
        """[[ a | break ]][[ b | date | break ]]"""
        """[[ c | date ]] [[ d | date ]]""",
        {
            "a": "b",
            "b": "2020-02-10",
            "c": "2019-10-26T22:00:00+00:00",
            "d": "not_a_date",
        },
    )
    assert result == "b\n10 februari 2020\n27 oktober 2019 "


def test_template_jsonpath():
    t = LegacyTemplate()
    result = t.render(
        """[[ j('$.a') ]] [[ j('$.b[0,1]') ]] [[- j('$.c') ]]""",
        {"a": "b", "b": ["c", "d", "e"]},
    )
    assert result == "b c, d"

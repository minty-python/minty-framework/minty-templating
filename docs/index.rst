Welcome!
========

Please see the menu on the right for more information about this package.

.. toctree::
    :maxdepth: 2
    :caption: Contents:

    README.rst
    modules.rst
    CONTRIBUTING.rst
    CONTRIBUTORS.rst


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
